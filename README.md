# Step project cards
## by Christina and Sergey Tverdokhleb and Sergey Peksymov

Sergey T:
 - Login modal, login service
 - Create\Update card modal, remote card update

Christina:
 - Visual part: html+css for filter and card
 - Interaction with server: creating/deleting card, getting all cards
 - Filter for cards
 - DragAndDrop
 
Sergey P:
 - Visual part: main design
 - Data classes, forms, card services