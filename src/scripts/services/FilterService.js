import CardService from "./CardService.js";
import {renderCards} from "./build.js";

class FilterService {
    constructor() {
    }

    async filter(ev) {
        ev.preventDefault();
        await CardService.loadCards()
        let allCards = CardService.cards;
        const form = document.getElementById('filter-form')

        for (const input of form) {
            if (input.name) {
                if (input.name === 'search') {
                    if (input.value !== '') {
                        allCards = allCards.filter(card => card.content.initials.toLowerCase().includes(input.value.toLowerCase()));
                    }
                }
                if (input.name === 'ClassName') {
                    if (input.value !== '') {
                        allCards = allCards.filter(card => card.className === input.value)
                    }
                }
                if (input.name === 'urgencyType') {
                    if (input.value !== '') {
                        allCards = allCards.filter(card => card.content.urgencyType === input.value)
                    }
                }
            }
        }
        renderCards(allCards);
    }
}

export default new FilterService();