import URL from './urls.js'
import LoginService from "./LoginService.js";
import * as Data from "../classes/data.js";


class CardService {
    constructor() {
        this._cards = [];
    }

    get cards() {
        return this._cards;
    }

    async createCard(visit) {
        let response = await fetch(URL.cardsURL, {
            method: 'POST',
            headers: {
                ...LoginService.getAuthorizationHeader(),
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(visit)
        });
        if (response.ok) {
            let body = await response.json();
            let card = mapCard(body);
            this._cards.push(card);
            return card;
        } else {
            throw new Error('Все плохо');
        }
    }

    async loadCards() {
        let response = await fetch(URL.cardsURL, {
            method: 'GET',
            headers: {
                ...LoginService.getAuthorizationHeader(),
            },
        });
        if (response.ok) {
            let cards = await response.json();
            this._cards = cards.map(card => mapCard(card));
        } else {
            throw new Error();
        }
    }

    async deleteCard(id) {
        let response = await fetch(`${URL.cardsURL}/${id}`, {
            method: 'DELETE',
            headers: {
                ...LoginService.getAuthorizationHeader(),
            },
        });
        if (response.ok) {
            let index = this._cards.findIndex(elem => elem.id === +id)
            this._cards.splice(index, 1);
        } else {
            throw new Error();
        }
    }

    async updateCard(id, visit) {
        let response = await fetch(`${URL.cardsURL}/${id}`, {
            method: 'PUT',
            headers: {
                ...LoginService.getAuthorizationHeader(),
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(visit)
        });
        if (response.ok) {
            let body = await response.json();
            let card = mapCard(body);
            let index = this._cards.findIndex(card => card.id === +id);
            this._cards[index] = card;
            console.log('Карточка успешно отредактирована');
            return card;
        } else {
            throw new Error();
        }
    }
}

function mapCard(card) {
    return new Data[card.content.className](card.id, card.content);

}

export default new CardService();