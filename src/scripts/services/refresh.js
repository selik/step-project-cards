import LoginService from "./LoginService.js";
import CardService from "./CardService.js";
import {renderCards} from "./build.js";

export function refreshLoginState() {
    if (LoginService.getToken()) {
        document.getElementById('btn-lgn').style.display = 'none';
        document.getElementById('btn-crt').style.display = '';
        CardService.loadCards()
            .then(() => renderCards(CardService.cards));
    } else {
        document.getElementById('btn-lgn').style.display = '';
        document.getElementById('btn-crt').style.display = 'none';
    }
}