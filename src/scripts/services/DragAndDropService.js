export function drag(el) {
    el.addEventListener('dragstart', (e) => {
        e.dataTransfer.setData('text/html', 'dragstart')
        el.dataset.coordX = e.offsetX;
        el.dataset.coordY = e.offsetY;
        el.dataset.x = el.dataset.x || el.offsetLeft;
        el.dataset.y = el.dataset.y || el.offsetTop;
        el.classList.toggle("active");
    })
}


export function drop() {
    const dropZone = document.getElementById('visitss')
    let zIndex = 0;

    dropZone.addEventListener('dragover', (e) => {
        e.preventDefault();
    });

    dropZone.addEventListener('drop', (e) => {
        let el = document.querySelector('.card-wrapper.active')
        el.style.zIndex = ++zIndex;
        el.style.top = (e.pageY - el.dataset.coordY - +el.dataset.y) + 'px';
        el.style.left = (e.pageX - el.dataset.coordX - +el.dataset.x) + 'px';
        el.classList.toggle("active");
    })
}