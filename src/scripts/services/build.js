import * as Data from '../classes/data.js';
import {CardEditModal} from "../modals/CardEditModal.js";
import CardService from "./CardService.js";
import {drag} from "./DragAndDropService.js";

const defaultLabel = '<div class="h6 main-color">Нет созданных записей</div>';

export function renderCards(some) {
    const visitContainer = document.getElementById('visitss')
    visitContainer.innerHTML = '';
    if (some.length === 0) {
        visitContainer.insertAdjacentHTML('beforeend', defaultLabel);
    } else {
        some.forEach(elem => renderCard(elem));
    }
}

export function renderCard(card) {
    let existingCard = document.getElementById(card.id);
    let renderedCard = buildCard(card);
    if (existingCard) {
        existingCard.outerHTML = renderedCard;
        drag(document.getElementById(card.id));
    } else {
        let visitContainer = document.getElementById('visitss');
        if (!visitContainer.querySelectorAll('.card-wrapper').length) {
            visitContainer.innerHTML = '';
        }
        visitContainer.insertAdjacentHTML('beforeend', renderedCard);
        let createdCard = visitContainer.lastElementChild
        drag(createdCard);
    }
}

export function prepareCardContainer() {
    const visitContainer = document.getElementById('visitss')
    visitContainer.addEventListener('click', (ev) => {
        let target = ev.target;

        let edit = target.closest('.js-edit-card');
        if (edit) {
            let id = edit.dataset.target;
            CardEditModal.editCard(id);
            $('#create-edit-modal').modal('show');
            return;
        }

        let remove = target.closest('.js-remove-card');
        if (remove) {
            let id = remove.dataset.target;
            CardService.deleteCard(id);
            removeCard(id);
        }
        let showMoreLess = target.closest('.show-more-less');
        if (showMoreLess.innerHTML ==="Показать больше") {
            showMoreLess.innerHTML = "Показать меньше"
        } else {
            showMoreLess.innerHTML = "Показать больше"
        }
    })
}

export function removeCard(cardId) {
    let existingCard = document.getElementById(cardId);
    if (existingCard) {
        existingCard.remove();
        const visitContainer = document.getElementById('visitss');
        if (!visitContainer.querySelectorAll('.card-wrapper').length) {
            visitContainer.innerHTML = defaultLabel;
        }
    }
}

function buildCard({id, className, content}) {
    return `
            <div id="${id}" class="card-wrapper" draggable="true">
                <div class="card card-edits">
                    <div class="d-flex justify-content-end">
                        <button type="button" class="close float-none js-edit-card" data-target="${id}">
                            <svg width="1em" height="0.6em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                <path fill-rule="evenodd"
                                      d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                            </svg>
                        </button>
        
                        <button type="button" class="close float-none mr-2 js-remove-card" aria-label="Close" data-target="${id}">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
        
                    <div class="card-body pt-0 pb-1 text-center">
                        <h5 class="card-title">${content.initials}</h5>
                        <p class="card-text">${Data[className].displayName}</p>
                        <a href="#" data-toggle="collapse" data-target="#collapseExample${id}"
                           class=" btn load-more-btn btn-light mb-1 py-1 showw">
                           <span class="show-more-less">Показать больше</span>
                           </a>
                    </div>
                    <div class="card-body pt-0 pb-1 collapse text-center " id="collapseExample${id}">
                        ${prepareContent(content)}
                    </div>
                </div>
            </div>
            `
}

function prepareContent(content) {
    let html = '';
    for (const key in content) {
        if (content.hasOwnProperty(key)) {
            let label = mapping[key];
            let value = content[key];
            if (key === 'className' || key === 'initials') {
                // do nothing, className or initials are not needed in block
                value = null;
            } else if (key === 'urgencyType') {
                value = urgencyMapping[value];
            }

            if (value) {
                html += `<div>
                            <p class="font-weight-bold my-0">${label}</p>
                            <span>${value}</span>
                        </div>`
            }
        }
    }
    return html;
}

const mapping = {
    goalOfVisit: 'Цель визита',
    shortDescription: 'Краткое описание',
    urgencyType: 'Срочность',
    lastVisitDate: 'Дата последнего визита',

    ageClient: 'Возраст',

    usualPressure: 'Обычное давление',
    bodyMassIndex: 'Индекс массы тела',
    pastDiseasesCardiovascularSystem: 'Перенесенные заболевания',
}

const urgencyMapping = {
    '1': 'Обычная',
    '2': 'Приоритетная',
    '3': 'Неотложная'
}

