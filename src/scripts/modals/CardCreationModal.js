import * as Tags from "../classes/tags.js";
import {Modal} from "./Modal.js";
import {VisitCardiologistForm} from "../forms/VisitCardiologistForm.js";
import {VisitDentistForm} from "../forms/VisitDentistForm.js";
import {VisitTherapistForm} from "../forms/VisitTherapistForm.js";

export class CardCreationModal extends Modal {
    constructor(btnId, props) {
        const body = new Tags.Element('div', {
            classList: ['divInline'],
            children: [
                new Tags.Element('div', {
                    classList: ['d-flex', 'mb-4', 'justify-content-between'],
                    children: [
                        new Tags.Element('div', {
                            classList: ['btn-group', 'dropright'],
                            children: [
                                new Tags.Button('button', {
                                    classList: ['btn', 'btn-secondary', 'dropdown-toggle'],
                                    dataList: {
                                        toggle: 'dropdown'
                                    },
                                    children: ['Выбрать врача']
                                }),
                                new Tags.Element('div', {
                                    classList: ['dropdown-menu'],
                                    children: [
                                        new Tags.Element('a', {
                                            classList: ['dropdown-item', 'cardiology'],
                                            dataList: {
                                                form: VisitCardiologistForm.name
                                            },
                                            attributes: {
                                                href: '#'
                                            },
                                            children: ['Кардиолог  <i class="fas fa-file-medical-alt"></i>'],
                                            handlers: {
                                                click: showForm
                                            }
                                        }),
                                        new Tags.Element('a', {
                                            classList: ['dropdown-item', 'dentistry'],
                                            dataList: {
                                                form: VisitDentistForm.name
                                            },
                                            attributes: {
                                                href: '#'
                                            },
                                            children: ['Стоматолог  <i class="fas fa-tooth"></i>'],
                                            handlers: {
                                                click: showForm
                                            }
                                        }),
                                        new Tags.Element('a', {
                                            classList: ['dropdown-item', 'therapy'],
                                            dataList: {
                                                form: VisitTherapistForm.name
                                            },
                                            attributes: {
                                                href: '#'
                                            },
                                            children: ['Терапевт  <i class="fas fa-user-md"></i>'],
                                            handlers: {
                                                click: showForm
                                            }
                                        })
                                    ]
                                })
                            ]
                        }),
                        new Tags.Element('span')
                    ]
                }),
                new Tags.Element('div', {
                    classList: ['js-create-body']
                })
            ]
        });
        super(btnId, body, props);
    }

    static renderModal() {
        return new CardCreationModal(['btn-crt'], {
            id: 'create-card-modal',
            title: 'Создание новой карточки'
        }).render();
    }

}

let forms = {VisitTherapistForm, VisitDentistForm, VisitCardiologistForm}

function showForm() {
    let span = this.closest('.d-flex').querySelector('span');
    span.innerHTML = this.innerHTML;
    let formType = this.dataset.form;

    let element = document.querySelector('.js-create-body');
    element.innerHTML = '';
    let form = new forms[formType]().render();
    form.dataset.formType = formType;
    element.append(form);
}