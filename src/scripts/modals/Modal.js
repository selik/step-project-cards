import * as Tags from "../classes/tags.js";

export class Modal extends Tags.Element {
    constructor(btnIds, body, props) {
        super('div', {
            id: props.id,
            classList: ['modal', 'fade'],
            attributes: {
                tabindex: '-1'
            },
            children: [
                new Tags.Element('div', {
                    classList: ['modal-dialog'],
                    children: [
                        new Tags.Element('div', {
                            classList: ['modal-content'],
                            children: [
                                new Tags.Element('div', {
                                    classList: ['modal-header'],
                                    children: [
                                        new Tags.Element('h5', {
                                            classList: ['modal-title'],
                                            children: [props.title]
                                        }),
                                        new Tags.Element('button', {
                                            classList: ['close'],
                                            dataList: {
                                                dismiss: 'modal'
                                            },
                                            children: [
                                                new Tags.Element('span', {
                                                    children: ['&times;']
                                                })
                                            ]
                                        })
                                    ]
                                }),
                                new Tags.Element('div', {
                                    classList: ['modal-body'],
                                    children: [body]
                                }),
                            ]
                        })
                    ]
                })
            ]
        })

        let modal = this;

        btnIds.forEach(btnId => {
            let btn = document.getElementById(btnId);
            btn.onclick = function () {
                document.getElementById(`${props.id}`).replaceWith(modal.render());
                $(`#${props.id}`).modal('show');
            }
        })
    }
}