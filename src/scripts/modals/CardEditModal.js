import * as Tags from "../classes/tags.js";
import {Modal} from "./Modal.js";
import {VisitCardiologistForm} from "../forms/VisitCardiologistForm.js";
import {VisitDentistForm} from "../forms/VisitDentistForm.js";
import {VisitTherapistForm} from "../forms/VisitTherapistForm.js";
import CardService from "../services/CardService.js";
import * as Data from "../classes/data.js";

let forms = {VisitTherapistForm, VisitDentistForm, VisitCardiologistForm};

export class CardEditModal extends Modal {
    constructor(btnId, props) {
        const body = new Tags.Element('div', {
            classList: ['divInline'],
            children: [
                new Tags.Element('div', {
                    classList: ['d-flex', 'mb-4', 'justify-content-end'],
                    children: [
                        new Tags.Element('span')
                    ]
                }),
                new Tags.Element('div', {
                    classList: ['js-edit-body']
                })
            ]
        });
        super(btnId, body, props);
    }

    static editCard(cardId) {
        let card = CardService.cards.find(c => c.id === +cardId);

        if (card) {
            let modal = document.querySelector('#create-edit-modal');
            modal.querySelector('.d-flex span').innerHTML = Data[card.className].displayName;

            let element = modal.querySelector('.js-edit-body');
            element.innerHTML = '';
            let formType = Data[card.className].name + 'Form';
            let form = new forms[formType]().renderCard(card);
            form.dataset.formType = formType;
            form.dataset.id = cardId;
            element.append(form);
        }
    }
}