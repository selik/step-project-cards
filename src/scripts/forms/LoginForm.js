import * as Tags from "../classes/tags.js";
import {Form} from "./Form.js";
import LoginService from "../services/LoginService.js";
import {refreshLoginState} from "../services/refresh.js";

export class LoginForm extends Form {
    constructor() {
        super({
            id: 'login-form',
            children: [
                new Tags.Element('div', {
                    classList: ['form-group'],
                    children: [
                        new Tags.Element('label', {
                            attributes: {
                                for: 'login-email',
                            },
                            children: ['Введите ваш Email']
                        }),
                        new Tags.Input('email', 'email', {
                            id: 'login-email'
                        }),
                        new Tags.Element('small', {
                            id: 'emailHelp',
                            classList: ['form-text', 'text-muted'],
                            children: ['Никому Ваш email мы не отдадим. Честно ;)']
                        })
                    ]
                }),
                new Tags.Element('div', {
                    classList: ['form-group'],
                    children: [
                        new Tags.Element('label', {
                            attributes: {
                                for: 'login-password',
                            },
                            children: ['Введите ваш пароль']
                        }),
                        new Tags.Input('password', 'password', {
                            id: 'login-password'
                        })
                    ]
                }),
                new Tags.Element('div', {
                    classList: ['form-group', 'd-flex', 'justify-content-end'],
                    children: [
                        new Tags.Button('button', {
                            classList: ['btn', 'btn-secondary', 'm-1'],
                            dataList: {
                                dismiss: 'modal'
                            },
                            children: ['Закрыть']
                        }),
                        new Tags.Button('submit', {
                            classList: ['btn', 'btn-primary', 'm-1'],
                            children: ['Войти'],
                        })
                    ]
                }),
                new Tags.Element('span', {
                    classList: ['error']
                })
            ],
            handlers: {
                submit: function (ev) {
                    ev.preventDefault();
                    const form = this;
                    form.querySelector('.error').innerHTML = '';
                    const result = {};
                    for (const input of form) {
                        if (input.name) {
                            result[input.name] = input.value;
                        }
                    }
                    LoginService.login(result.email, result.password)
                        .then(() => {
                            refreshLoginState()
                            form.querySelector('.btn-secondary').click();
                        })
                        .catch((e) => form.querySelector('.error').append(e));
                }
            }
        });
    }
}

