import * as Tags from "../classes/tags.js";
import {Form} from "./Form.js";
import CardService from "../services/CardService.js";
import {renderCard} from "../services/build.js";

export class VisitForm extends Form {
    constructor(fields) {

        const _fields = [
            new InputWrapper('Цель визита', new Tags.Input('text', 'goalOfVisit', {})),
            new InputWrapper('Краткое описание', new Tags.TextArea('shortDescription')),
            new InputWrapper('Срочность', new Tags.Select('urgencyType', [{
                label: 'Выберите срочность',
                selected: true,
                disabled: true,
                value: ''
            }, {
                label: 'Обычная',
                value: 1
            }, {
                label: 'Приоритетная',
                value: 2
            }, {
                label: 'Неотложная',
                value: 3
            }], {
                classList: ['custom-select']
            })),
            new InputWrapper('ФИО', new Tags.Input('text', 'initials',)),
            ...(fields || [])
        ]

        super({
            children: [
                ..._fields,
                new Tags.Element('div', {
                    classList: ['form-group', 'd-flex', 'justify-content-end'],
                    children: [
                        new Tags.Button('button', {
                            classList: ['btn', 'btn-secondary', 'm-1'],
                            dataList: {
                                dismiss: 'modal'
                            },
                            children: ['Закрыть']
                        }),
                        new Tags.Button('submit', {
                            classList: ['btn', 'btn-primary', 'm-1'],
                            children: ['Принять'],
                        })
                    ]
                }),
                new Tags.Element('span', {
                    classList: ['error']
                })
            ],
            handlers: {
                submit: function (ev) {
                    ev.preventDefault();
                    const form = this;
                    form.querySelector('.error').innerHTML = '';
                    const card = {
                        className: form.dataset.formType.replace('Form', '')
                    };
                    for (const input of form) {
                        if (input.name) {
                            card[input.name] = input.value;
                        }
                    }
                    if (form.dataset.id) {
                        CardService.updateCard(form.dataset.id, card)
                            .then((card) => {
                                renderCard(card);
                                form.querySelector('.btn-secondary').click();
                            })
                            .catch(e => form.querySelector('.error').append(e));
                    } else {
                        CardService.createCard(card)
                            .then((card) => {
                                renderCard(card);
                                form.querySelector('.btn-secondary').click();
                            })
                            .catch(e => form.querySelector('.error').append(e));
                    }
                }
            }
        });
    }

    renderCard(card) {
        let render = this.render();
        if (card) {
            for (const el of render) {
                el.value = card.content[el.name]
            }
        }
        return render;
    }

    static createWrapper(label, input) {
        return new InputWrapper(label, input);
    }
}

class InputWrapper extends Tags.Element {
    constructor(label, input) {
        super('div', {
            classList: ['input-group', 'mb-3'],
            children: [
                new Tags.Element('div', {
                    classList: ['input-group-prepend'],
                    children: [
                        new Tags.Element('span', {
                            classList: ['input-group-text'],
                            children: [label]
                        })
                    ]
                }),
                input
            ]
        });
    }
}