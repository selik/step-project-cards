import {VisitForm} from "./VisitForm.js";
import {Input, TextArea} from "../classes/tags.js";

export class VisitCardiologistForm extends VisitForm {
    constructor() {
        const fields = [
            VisitForm.createWrapper('Обычное давление', new Input('text', 'usualPressure')),
            VisitForm.createWrapper('Индекс массы тела', new Input('text', 'bodyMassIndex')),
            VisitForm.createWrapper('Перенесенные заболевания сердечно-сосудистой системы', new TextArea('pastDiseasesCardiovascularSystem')),
            VisitForm.createWrapper('Возраст клиента', new Input('text', 'ageClient')),
        ]

        super(fields);
    }

}