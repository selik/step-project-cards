import {VisitForm} from "./VisitForm.js";
import {Input} from "../classes/tags.js";

export class VisitTherapistForm extends VisitForm {
    constructor() {
        const fields = [
            VisitForm.createWrapper('Возраст клиента', new Input('text', 'ageClient')),
        ]

        super(fields);
    }

}