import {VisitForm} from "./VisitForm.js";
import {Input} from "../classes/tags.js";

export class VisitDentistForm extends VisitForm {
    constructor() {
        const fields = [
            VisitForm.createWrapper('Дата последнего посещения', new Input('date', 'lastVisitDate'))
        ]

        super(fields);
    }

}