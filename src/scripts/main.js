import {refreshLoginState} from "./services/refresh.js";
import {Modal} from "./modals/Modal.js";
import {CardCreationModal} from "./modals/CardCreationModal.js";
import {CardEditModal} from "./modals/CardEditModal.js";
import {LoginForm} from "./forms/LoginForm.js";
import FilterService from "./services/FilterService.js";
import {drop} from "./services/DragAndDropService.js";
import {prepareCardContainer} from "./services/build.js";

document.addEventListener('DOMContentLoaded', () => {
    refreshLoginState();

    document.body.insertAdjacentElement('beforeend', new Modal(['btn-lgn'], new LoginForm(), {
        id: 'login-modal',
        title: 'Login',
    }).render());

    document.body.insertAdjacentElement('beforeend', CardCreationModal.renderModal());

    document.body.insertAdjacentElement('beforeend', new CardEditModal([], {
        id: 'create-edit-modal',
        title: 'Edit visit'
    }).render());

    document.getElementById('btn-filter').addEventListener('click', function (ev) {
        FilterService.filter(ev)
    });
    prepareCardContainer()
    drop()
});