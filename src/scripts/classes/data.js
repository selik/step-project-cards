class Visit {
    constructor(id, content) {
        this.id = id;
        this.className = content.className;
        this.content = {
            goalOfVisit: content.goalOfVisit,
            shortDescription: content.shortDescription,
            urgencyType: content.urgencyType,
            initials: content.initials
        }
    }
}

export class VisitCardiologist extends Visit {
    constructor(id, content) {
        super(id, content);
        this.content = {
            ...this.content,
            usualPressure: content.usualPressure,
            bodyMassIndex: content.bodyMassIndex,
            pastDiseasesCardiovascularSystem: content.pastDiseasesCardiovascularSystem,
            ageClient: content.ageClient
        }
    }
}

export class VisitDentist extends Visit {
    constructor(id, content) {
        super(id, content);
        this.content = {
            ...this.content,
            lastVisitDate: content.lastVisitDate
        }
    }
}

export class VisitTherapist extends Visit {
    constructor(id, content) {
        super(id, content);
        this.content = {
            ...this.content,
            ageClient: content.ageClient
        }
    }
}

VisitCardiologist.displayName = 'Кардиолог';
VisitDentist.displayName = 'Стоматолог';
VisitTherapist.displayName = 'Терапевт';