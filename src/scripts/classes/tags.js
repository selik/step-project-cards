export class Element {
    constructor(tag, props) {
        this._tagName = tag;
        this._props = props || {};
    }

    render() {
        let element = document.createElement(this._tagName);

        if (this._props.id) {
            element.id = this._props.id;
        }

        if (this._props.classList) {
            element.classList.add(...this._props.classList);
        }

        let dataList = this._props.dataList;
        if (dataList) {
            for (const data in dataList) {
                if (dataList.hasOwnProperty(data)) {
                    element.dataset[data] = dataList[data];
                }
            }
        }

        let attributes = this._props.attributes;
        if (attributes) {
            for (const attribute in attributes) {
                if (attributes.hasOwnProperty(attribute)) {
                    element.setAttribute(attribute, attributes[attribute]);
                }
            }
        }

        let children = this._props.children;
        if (children) {
            children.forEach(ch => {
                if (ch instanceof Element) {
                    element.append(ch.render())
                } else {
                    element.insertAdjacentHTML('beforeend', ch);
                }
            });
        }

        let handlers = this._props.handlers;
        if (handlers) {
            for (let handler in handlers) {
                if (handlers.hasOwnProperty(handler)) {
                    element[`on${handler}`] = handlers[handler].bind(element);
                }
            }
        }

        return element;
    }
}

class FormElement extends Element {
    constructor(tag, props = {}) {
        if (!props.classList) {
            props.classList = ['form-control'];
        }

        props.attributes = {
            ...props.attributes,
            required: true
        };

        super(tag, props);
    }

}

export class Input extends FormElement {
    constructor(type, name, props) {
        super('input', props);
        this._type = type;
        this._name = name;
    }

    render() {
        let input = super.render();
        input.type = this._type;
        input.name = this._name;
        return input;
    }
}

export class Button extends FormElement {
    constructor(type, props) {
        super('button', props);
        this._type = type;
    }


    render() {
        let button = super.render();
        button.type = this._type;
        return button;
    }
}

export class Select extends FormElement {
    constructor(name, options, props = {}) {
        const children = [];

        options.forEach(op => {
            children.push(new Element('option', {
                attributes: {
                    ...(op.value !== undefined && {value: op.value}),
                    ...(op.selected && {selected: op.selected}),
                    ...(op.disabled && {disabled: op.disabled}),
                },
                children: [op.label]
            }))
        })

        props.children = children;

        super('select', props);
        this._name = name;
    }


    render() {
        let select = super.render();
        select.name = this._name;
        return select;
    }
}

export class TextArea extends FormElement {
    constructor(name, props) {
        super('textarea', props);
        this._name = name;
    }


    render() {
        let area = super.render();
        area.name = this._name;
        return area;
    }
}
